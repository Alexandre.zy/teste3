package Test;

import static org.junit.Assert.*;

import modelo.Aluno;
import modelo.ControleAluno;

import org.junit.Test;
import org.junit.Before;



public class testControleAluno {
    
    private ControleAluno controleTest;
    
    @Before
    public void setUp() throws Exception {
        ControleAluno controleTest = new ControleAluno();
    }

    @Test
	public void testAdicionar() {
		ControleAluno controleTest = new ControleAluno();
		Aluno alunoexemplo = new Aluno();
		assertEquals(controleTest.adicionaAluno(alunoexemplo), alunoexemplo);
    }
    @Test
	public void testRemover() {
		ControleAluno controleTest = new ControleAluno();
		Aluno alunoexemplo = new Aluno();
		assertEquals(controleTest.removeAluno(alunoexemplo), alunoexemplo);
    }
    @Test
	public void testPesquisarMatricula() {
		ControleAluno controleTest = new ControleAluno();
		Aluno alunoexemplo = new Aluno();
		alunoexemplo.setMatriculaAluno("123456");
		controleTest.adicionaAluno(alunoexemplo);
		
		assertEquals(controleTest.pesquisaMatriculaAluno("123456"), alunoexemplo);
    }
}
