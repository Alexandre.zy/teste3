package modelo;
/* A classe ControleAluno cria um controle de instâncias da classe Aluno, 
criando uma lista de alunos e sendo responsável pelo gerenciamento dessa lista */
import java.util.ArrayList;
public class ControleAluno {
	//Atributo
	private ArrayList<Aluno> listaAlunos;
	
	//Construtor
	public ControleAluno() {
		listaAlunos = new ArrayList<Aluno>();
		
	}
	
	//Métodos
	public Aluno adicionaAluno(Aluno aluno) {
		listaAlunos.add(aluno);
		//System.out.println("Aluno cadastrado no sistema com sucesso!\n");
		return aluno;
	}
	
	public Aluno removeAluno(Aluno aluno) {
		listaAlunos.remove(aluno);
		//System.out.println("Aluno cadastrado no sistema com sucesso!\n");
		return aluno;
	}
	
	public Aluno pesquisaNomeAluno(String nomePesquisado) {
		for(Aluno aluno : listaAlunos) {
			if(aluno.getNomeAluno().equalsIgnoreCase(nomePesquisado)) return aluno;
		}
		String mensagem = "Aluno pesquisado";
		return null;
	}
	
	public Aluno pesquisaMatriculaAluno(String matriculaPesquisada) {
		for(Aluno aluno : listaAlunos) {
			if(aluno.getMatriculaAluno().equalsIgnoreCase(matriculaPesquisada)) return aluno;
		}
		return null;
	}
}