package modelo;
//Classe responsável por gerenciar uma lista de classes e adicionar alunos às disciplinas da lista
import java.util.ArrayList;
public class ControleDisciplina {
	//Atributo
	private ArrayList<Disciplina> listaDisciplinas;
	
	//Construtor
	public ControleDisciplina() {
		listaDisciplinas = new ArrayList<Disciplina>();
	}
	
	//Métodos
	public Disciplina adicionaDisciplina(Disciplina disciplina) {
		listaDisciplinas.add(disciplina);
		System.out.println("Disciplina adicionada com sucesso!");
		return disciplina;
	}
	
	public String adicionaAlunoADisciplina(String disciplinaPesquisada, Aluno alunoPesquisado) {
		for(Disciplina disciplina : listaDisciplinas) {
			if(disciplina.getNomeDisciplina().equalsIgnoreCase(disciplinaPesquisada)){
					disciplina.adicionaAluno(alunoPesquisado);
			}
		}
		return disciplinaPesquisada;
	}
	
	public String exibirDisciplinas() {
		for(Disciplina disciplina : listaDisciplinas) {
			int i = 1;
			String Retorno = i+". "+disciplina.getCodigoDisciplina()+" - "+disciplina.getNomeDisciplina();
			System.out.println(Retorno);
			i++;
		}
		return disciplina.getCodigoDisciplina()+" - "+disciplina.getNomeDisciplina();
	}
	
	public void exibirAlunosMatriculados(String disciplinaPesquisada) {
		for(Disciplina disciplina : listaDisciplinas) {
			if(disciplina.getNomeDisciplina().equals(disciplinaPesquisada)){
				disciplina.exibirAlunosMatriculados();
			}
			break;	
		}
	}
}